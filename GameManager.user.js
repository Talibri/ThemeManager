// ==UserScript==
// @name         Talibri - Game Manager
// @namespace    Xmitty
// @version      0.11a
// @description  A singular script to help manage themes and add new features.
// @author       Xmitty
// @match        https://talibri.com/*
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.js
// @grant        none
// ==/UserScript==

//Globals
var pickersText = new Array();
var pickersBackground = new Array();
var pickersBorder = new Array();

$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />');
$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js"></script>');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css" />');
//$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>');
//$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css" />');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/table.css" />');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/divider.css" />');
$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/checkbox.js"></script>');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/checkbox.css" />');
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/input.css" />');


var start = {
	
	initialize: function() {
		
		storage.load();
		
		start.addMenu();
		start.addTheme();
		
		window.setInterval(start.addMenu, 500);
		window.setInterval(start.addTheme, 500);	
	},

	addMenu: function() {
		
		if($(".gameManagerDropdown").length>0)
		{
			return;
		}
		var menuHtml = '<li class="gameManagerDropdown">';
			menuHtml += '<a class="gameManagerDropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded=false">';
			menuHtml += 'Game Manager<span class="caret"></span></a>';
			menuHtml += '<ul class="dropdown-menu">';
			menuHtml += '<li><a id="themeManager">Theme Manager</a></li>';
			menuHtml += '</ul>';
			menuHtml += '</li>';
		$('.navbar-nav').first().append(menuHtml);
		$("#themeManager").on('click', function () { theme.showUI(); });
	},
	
	addTheme: function() {
			
		if($("#themeDialog").length>0)
		{
			return;
		}
			
		var Html = "";
		Html += '<div id="themeDialog" title="Themes" style="height: 100%; display: none">';
		Html += '<div id="tabs_in_dialog"><ul>';
		Html += '<li style="margin-right: 15px"><a href="#tabs-5">Instructions</a></li><li style="margin-right: 15px"><a href="#tabs-6">Save/Settings</a></li>'; 
		Html += '<li><a href="#tabs-1">Backgrounds</a></li><li><a href="#tabs-2">Texts</a></li>';  
		Html += '<li><a href="#tabs-3">Borders</a></li><li><a href="#tabs-4">Buttons</a></li>';  
		//***Background Colors***
		Html += '<div id="tabs-1" style="position: absolute; height: 90%; width: 97%; top: 50px">';
		Html += '<div class="ui horizontal inverted divider">MAIN BACKGROUNDS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlBackground("mainBg"); 
		Html += theme.htmlBackground("mainHeaderBg");
		Html += theme.htmlBackground("mainFooterBg");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">CHAT BACKGROUNDS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlBackground("chatMainBg"); 
		Html += theme.htmlBackground("chatFooterBg"); 
		Html += theme.htmlBackground("chatHeadingBg");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">PANEL BACKGROUNDS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlBackground("panelHeaderBg");
		Html += theme.htmlBackground("panelBodyBg");
		Html += theme.htmlBackground("panelFooterBg");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div>';
		//***Text Colors***
		Html += '<div id="tabs-2" style="position: absolute; height: 90%; width: 97%; top: 50px">';
		Html += '<div class="ui horizontal inverted divider">MAIN TEXTS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlText("normalText"); 
		Html += theme.htmlText("mainHeaderText");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">CHAT TEXTS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlText("chatName");
		Html += theme.htmlText("adminName");
		Html += theme.htmlText("chatText");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div>';
		//***Border Colors***
		Html += '<div id="tabs-3" style="position: absolute; height: 90%; width: 97%; top: 50px">';
		Html += '<div class="ui horizontal inverted divider">MAIN BORDERS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlBorder("mainHeaderBorder");
		Html += theme.htmlBorder("mainFooterBorder");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">CHAT BORDERS</div>';
		Html += '<table class="ui selectable compact inverted table"><thead><tr><th class="three wide">Description</th><th class="one wide">Enabled</th><th class="one wide">Pick Color</th><th class="two wide">Current Value</th><th class="two wide">Example</th><th class="two wide">Default Value</th></tr></thead><tbody>';
		Html += theme.htmlBorder("chatMainBorder");
		Html += '</tbody></table>';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div>';
		//**Button Colors***
		Html += '<div id="tabs-4" style="position: absolute; height: 90%; width: 97%; top: 50px">';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div>';
		//**Instructions
		Html += '<div id="tabs-5" style="position: absolute; height: 90%; width: 97%; top: 50px">';
		Html += '<div class="ui horizontal inverted divider">BASIC INSTRUCTIONS</div>';
		Html += '<p style="color: rgb(200,200,200)">Choose the Tab for the Element you want to change. Pick the color, the example will automatically update. Turning off the Enable sets it to default, but keeps override. HIT SAVE AND RELOAD AFTER CHANGES.</p>';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div>';
		//**Settings
		Html += '<div id="tabs-6" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Settings / Reset
		Html += '<div id="wrapper" style="position: relative; width: 100%; height: 100%; float: left">';
		Html += '<div class="ui horizontal inverted divider">SAVE</div>';
		Html += '<button type="button" id="themeSaveBtn">Save Theme Data (Reload To Show)</button>';
		Html += '<div class="ui horizontal inverted divider">EXPORT</div>';
		Html += '<div class="ui inverted input" style="padding-right: 15px"><input id="exportInput" type="text" placeholder="Data Will Appear Here"></div>';
		Html += '<button type="button" id="themeExportBtn">Export Theme Data</button>';
		Html += '<div class="ui horizontal inverted divider">IMPORT</div>';
		Html += '<div class="ui inverted input" style="padding-right: 15px"><input id="importInput" type="text" placeholder="Input Paste Here"></div>';
		Html += '<button type="button" id="themeImportBtn">Import Theme Data</button>';
		Html += '<div class="ui horizontal inverted divider">RESET</div>';
		Html += '<button type="button" id="themeResetBtn">Reset All Theme Data To Default</button>';
		Html += '<div class="ui horizontal inverted divider">END</div>';
		Html += '</div></div>';
		Html += '</div></div>';
		
		$('body').append(Html);
		
		$("#themeSaveBtn").on('click', function () { storage.save(); });
		$("#themeResetBtn").on('click', function () { storage.reset(); });
		$("#themeExportBtn").on('click', function () { storage.exportTheme(); });
		$("#themeImportBtn").on('click', function () { storage.importTheme(); });
		
		start.initPickers();
		start.initEnabled();
	},
	
	initPickers: function() {
		for(i=0; i<pickersText.length; i++) {
			var nameDot = '.'+pickersText[i];
			var name = pickersText[i];
			$(nameDot).spectrum({
				appendTo: $("#themeDialog"),
				showInput: true,
				showAlpha: true,
				preferredFormat: "rgb",
				showInitial: true,
				showPalette: true,
				palette: [
					["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
					["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
					["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
					["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
					["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
					["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
					["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
					["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
				],
				change: function(color) {
					var a = $(this).attr('class');
					var b = 'value'+a;
					var c = document.getElementById(b);
					var d = 'example'+a;
					var e = document.getElementById(d);
					$(c).css('background-color',color.toRgbString());  
					c.value = color;
					$(e).css('color',color.toRgbString()); 
					storage.structure.data.themeData[a].overrideColor = s.value;
				},
				move: function(color) {
					var q = $(this).attr('class');
					var r = 'value'+q;
					var s = document.getElementById(r);
					var t = 'example'+q;
					var u = document.getElementById(t);
					$(s).css('background-color',color.toRgbString());  
					s.value = color;
					$(u).css('color',color.toRgbString()); 
					storage.structure.data.themeData[q].overrideColor = s.value;
				}
			});
		}
		for(i=0; i<pickersBackground.length; i++) {
			var nameDot = '.'+pickersBackground[i];
			var name = pickersBackground[i];
			$(nameDot).spectrum({
				appendTo: $("#themeDialog"),
				showInput: true,
				showAlpha: true,
				preferredFormat: "rgb",
				showInitial: true,
				showPalette: true,
				palette: [
					["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
					["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
					["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
					["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
					["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
					["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
					["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
					["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
				],
				change: function(color) {
					var a = $(this).attr('class');
					var b = 'value'+a;
					var c = document.getElementById(b);
					var d = 'example'+a;
					var e = document.getElementById(d);
					$(c).css('background-color',color.toRgbString());  
					c.value = color;
					$(e).css('background-color',color.toRgbString()); 
					storage.structure.data.themeData[a].overrideColor = s.value;
				},
				move: function(color) {
					var q = $(this).attr('class');
					var r = 'value'+q;
					var s = document.getElementById(r);
					var t = 'example'+q;
					var u = document.getElementById(t);
					$(s).css('background-color',color.toRgbString());  
					s.value = color;
					$(u).css('background-color',color.toRgbString()); 
					storage.structure.data.themeData[q].overrideColor = s.value;
				}
			});
		}
		for(i=0; i<pickersBorder.length; i++) {
			var nameDot = '.'+pickersBorder[i];
			var name = pickersBorder[i];
			$(nameDot).spectrum({
				appendTo: $("#themeDialog"),
				showInput: true,
				showAlpha: true,
				preferredFormat: "rgb",
				showInitial: true,
				showPalette: true,
				palette: [
					["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
					["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
					["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
					["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
					["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
					["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
					["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
					["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
				],
				change: function(color) {
					var a = $(this).attr('class');
					var b = 'value'+a;
					var c = document.getElementById(b);
					var d = 'example'+a;
					var e = document.getElementById(d);
					$(c).css('background-color',color.toRgbString());  
					c.value = color;
					$(e).css('border-color',color.toRgbString()); 
					storage.structure.data.themeData[a].overrideColor = s.value;
				},
				move: function(color) {
					var q = $(this).attr('class');
					var r = 'value'+q;
					var s = document.getElementById(r);
					var t = 'example'+q;
					var u = document.getElementById(t);
					$(s).css('background-color',color.toRgbString());  
					s.value = color;
					$(u).css('border-color',color.toRgbString()); 
					storage.structure.data.themeData[q].overrideColor = s.value;
				}
			});
		}
	},
	
	initEnabled: function() {	
		for(i=0; i<pickersText.length; i++) {
			var name = '#checkbox'+pickersText[i];
			if (storage.structure.data.themeData[pickersText[i]].enabledBool == "True") {
				$(name).checkbox('set checked');
			}
			$(name).checkbox().first().checkbox({
				onChecked: function() {
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "True";
				},
				onUnchecked: function() {
					console.log($(this).parent().attr('id'));
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "False";
				}
			});
		}
		
		for(i=0; i<pickersBackground.length; i++) {
			var name = '#checkbox'+pickersBackground[i];
			if (storage.structure.data.themeData[pickersBackground[i]].enabledBool == "True") {
				$(name).checkbox('set checked');
			}
			$(name).checkbox().first().checkbox({
				onChecked: function() {
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "True";
				},
				onUnchecked: function() {
					console.log($(this).parent().attr('id'));
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "False";
				}
			});
		}
		
		for(i=0; i<pickersBorder.length; i++) {
			var name = '#checkbox'+pickersBorder[i];
			if (storage.structure.data.themeData[pickersBorder[i]].enabledBool == "True") {
				$(name).checkbox('set checked');
			}
			$(name).checkbox().first().checkbox({
				onChecked: function() {
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "True";
				},
				onUnchecked: function() {
					storage.structure.data.themeData[$(this).parent().attr('id').replace('checkbox','')].enabledBool = "False";
				}
			});
		}
	}
};

var storage = {
	storageKey: 'GameManager',
	structure: {data: {themeData: {}}},
	
	save: function() {
		localStorage.setItem(storage.storageKey, JSON.stringify(storage.structure));
		
		console.log("Saving Storage.");
		console.log(storage.structure);
	},
	
	load: function() {
		storage.set();
		var loadedData = localStorage.getItem(storage.storageKey);
		if (loadedData !== null) {
			GMData = JSON.parse(loadedData);	
			Object.keys(storage.structure.data.themeData).forEach(function(key) {
				if (GMData.data.themeData[key] !== storage.structure.data.themeData[key] && GMData.data.themeData[key] !== undefined) {
					storage.structure.data.themeData[key] = GMData.data.themeData[key];
				}
			});

			theme.load();
		} else {		
			storage.reset();
			storage.save();	
			theme.load();
		}
	},
	
	reset: function() {
		storage.structure = JSON.parse(JSON.stringify(theme.defaults));
		console.log(JSON.stringify(storage.structure));
		storage.save();
	},
	
	set: function() {
		storage.structure = JSON.parse(JSON.stringify(theme.defaults));
		console.log(JSON.stringify(storage.structure));
	},
	
	exportTheme: function() {
		var compressed = LZString.compress(JSON.stringify(storage.structure));
		$("#exportInput").val(compressed);
	},
	
	importTheme: function() {
		var decompressed = LZString.decompress($("#importInput").val());
		storage.structure = JSON.parse(decompressed);
	}
};

var theme = {
	
	defaults: { "data" : { "themeData" : {
		
		//***Chat Panel***
		//Chat Colors - Text
		"chatText": {"name": "#messages .card-text", "defaultColor": "rgb(0, 0, 0)", "overrideColor": "rgb(0, 0, 0)", "description": "Chat :: Text Color", "enabledBool": "True"},
		"adminName": {"name": "#messages .admin", "defaultColor": "rgb(255, 0, 0)", "overrideColor": "rgb(255, 0, 0)", "description": "Admin Name :: Text Color", "enabledBool": "True"},
		"chatName": {"name": ".text-muted>a", "defaultColor": "rgb(65, 131, 196)", "overrideColor": "rgb(65, 131, 196)", "description": "Chat User Name :: Text Color", "enabledBool": "True"},
		//Chat Colors - Background
		"chatMainBg": {"name": ".panel.panel-primary.main-chat-panel", "defaultColor": "rgba(255, 255, 255, 0.75)", "overrideColor": "rgba(255, 255, 255, 0.75)", "description": "Chat Box :: Background Color", "enabledBool": "True"},
		"chatFooterBg": {"name": ".panel.panel-primary.main-chat-panel>.panel-footer", "defaultColor": "rgba(245,245,245,0.7)", "overrideColor": "rgba(245,245,245,0.7)", "description": "Chat Panel - Footer :: Background Color", "enabledBool": "True"},
		"chatHeadingBg": {"name": ".panel.panel-primary.main-chat-panel>.panel-heading", "defaultColor": "rgb(51, 122, 183)", "overrideColor": "rgb(51, 122, 183)", "description": "Chat Panel - Heading :: Background Color", "enabledBool": "True"},
		//Chat  Colors - Border
		"chatMainBorder": {"name": ".panel.panel-primary.main-chat-panel", "defaultColor": "rgb(65, 131, 196)", "overrideColor": "rgb(65, 131, 196)", "description": "Chat Box :: Main Border Color", "enabledBool": "True"},

		//***Main Page***
		//Page - Background
		"mainBg": {"name": "body>.container-fluid:first-of-type", "defaultColor": "rgb(0, 0, 0)", "overrideColor": "rgb(0, 0, 0)", "description": "Main :: Background Color", "enabledBool": "True"},
		//Page - Text
		"normalText": {"name": "body", "defaultColor": "rgb(0, 0, 0)", "overrideColor": "rgb(0, 0, 0)", "description": "Main :: Text Color", "enabledBool": "True"},
		
		//***Main Header***
		//Header Color - Background
		"mainHeaderBg": {"name": ".navbar.navbar-default.navbar-fixed-top", "defaultColor": "rgb(248, 248, 248)", "overrideColor": "rgb(248, 248, 248)", "description": "Main Heading :: Background Color", "enabledBool": "True"},
		//Header Color - Text
		"mainHeaderText": {"name": ".navbar-nav>li>a", "defaultColor": "rgb(119, 119, 119)", "overrideColor": "rgb(119, 119, 119)", "description": "Main Heading :: Text Color", "enabledBool": "True"},
		//Header Color - Border
		"mainHeaderBorder": {"name": ".navbar.navbar-default.navbar-fixed-top", "defaultColor": "rgb(231, 231, 231)", "overrideColor": "rgb(231, 231, 231)", "description": "Main Heading :: Border Color", "enabledBool": "True"},
		
		//**Main Footer***
		//Footer Color - Background	
		"mainFooterBg": {"name": ".navbar.navbar-default.navbar-fixed-bottom", "defaultColor": "rgb(248, 248, 248)", "overrideColor": "rgb(248, 248, 248)", "description": "Main Footer :: Background Color", "enabledBool": "True"},
		//Footer Color - Border
		"mainFooterBorder": {"name": ".navbar.navbar-default.navbar-fixed-bottom", "defaultColor": "rgb(231, 231, 231)", "overrideColor": "rgb(231, 231, 231)", "description": "Main Footer :: Border Color", "enabledBool": "True"},
		
		//**Panels***
		//General Panels - Background
		"panelHeaderBg": {"name": ".panel-heading", "defaultColor": "rgb(223, 240, 216)", "overrideColor": "rgb(223, 240, 216)", "description": "Default Panel Header :: Background Color", "enabledBool": "True"},
		"panelBodyBg": {"name": ".panel-body", "defaultColor": "rgba(255, 255, 255, 0.75)", "overrideColor": "rgba(255, 255, 255, 0.75)", "description": "Default Panel Body :: Background Color", "enabledBool": "True"},
		"panelFooterBg": {"name": ".panel-footer", "defaultColor": "rgba(245, 245, 245, 0.7)", "overrideColor": "rgba(245, 245, 245, 0.7)", "description": "Default Panel Footer :: Background Color", "enabledBool": "True"},

		
	}, "VersionInfo": {"Version": 1, "Date": "4/13/2018"}}},
	
	showUI: function() {
		$("#themeDialog").tabs().dialog({
			width: 1400, 
			height: 800,
			modal: true,
			resizeStop: theme.myResize,
			open: function (){
				//$(this).parent().children('.ui-dialog-titlebar').remove();
			},
		});
		$(".gameManagerDropdown-toggle").dropdown("toggle");		
	},
	
	myResize: function() {
		$(this).height($(this).parent().height() - $(this).prev('.ui-dialog-titlebar').height() - 34);
		$(this).width($(this).prev('.ui-dialog-titlebar').width() + 2);
	},
	
	load: function()
	{
		//***Denotes Good Implementation***
		
		//***Game Manager Style - Modal***
		addGlobalStyle('.ui-tabs-panel, .ui-dialog-titlebar { background-color: rgb(80, 80, 80) !important; }');
		addGlobalStyle('.ui-tabs-nav, .ui-tabs, .ui-dialog, .ui-tabs-tab { background-color: rgb(40, 40, 40) !important; }');
		addGlobalStyle('.ui-dialog-title, .ui-tabs-anchor { color: rgb(200, 200, 200) !important; }');
		addGlobalStyle('.ui-tabs-nav.ui-corner-all.ui-helper-reset.ui-helper-clearfix.ui-widget-header { height: 100% !important; }');
		addGlobalStyle('.ui-tabs-panel.ui-corner-bottom.ui-widget-content { background-color: rgba(0,0,0,0) !important; }');
		
		//***Chat Panel***
		//Chat Colors - Text
		if(storage.structure.data.themeData.chatText.enabledBool == "True") addGlobalStyle('#messages .card-text { color: '+storage.structure.data.themeData.chatText.overrideColor+'; overflow-wrap:break-word;}');
		if(storage.structure.data.themeData.adminName.enabledBool == "True") addGlobalStyle('#messages .admin {background-color: '+storage.structure.data.themeData.adminName.overrideColor+'; color: black !important;}');
		if(storage.structure.data.themeData.chatName.enabledBool == "True") addGlobalStyle('.text-muted>a { color: '+storage.structure.data.themeData.chatName.overrideColor+' !important; }');
		//Chat Colors - Background
		if(storage.structure.data.themeData.chatMainBg.enabledBool == "True") addGlobalStyle('.panel.panel-primary.main-chat-panel { background-color: '+storage.structure.data.themeData.chatMainBg.overrideColor+' !important;}');
		if(storage.structure.data.themeData.chatFooterBg.enabledBool == "True") addGlobalStyle('.panel.panel-primary.main-chat-panel>.panel-footer { background-color: '+storage.structure.data.themeData.chatFooterBg.overrideColor+' !important;}');
		if(storage.structure.data.themeData.chatHeadingBg.enabledBool == "True") addGlobalStyle('.panel.panel-primary.main-chat-panel>.panel-heading { background-color: '+storage.structure.data.themeData.chatHeadingBg.overrideColor+' !important;}');
		//Chat Colors - Border
		if(storage.structure.data.themeData.chatMainBorder.enabledBool == "True") addGlobalStyle('.panel.panel-primary.main-chat-panel { border-color: '+storage.structure.data.themeData.chatMainBorder.overrideColor+' !important;}');
		//Chat Styles
		addGlobalStyle('.main-chat-panel.panel-heading { height:37px !important; }');
		addGlobalStyle('.panel.panel-primary.main-chat-panel { border-radius: 0px !important; }'); 
		addGlobalStyle('body>.container-fluid:first-of-type>div.row>div.col-xs-3 { padding-left:0 !important; height:calc(100vh - 228px) !important; position: fixed !important; width:20%; }');
		addGlobalStyle('#messages { height:calc(100vh - 260px) !important; }');
		addGlobalStyle('.main-page { margin-left:20% !important; width:80% !important; margin-right:0 !important; padding-right:0 !important; margin-bottom:75px; }');
		addGlobalStyle('.main-chat-panel .form-group>br,.main-chat-panel .form-group>.text-muted {display:none; }');
		$('#main-chat-text-area').attr('placeholder','Remember: Hudy is always watching.');
		addGlobalStyle('.main-chat-panel .form-group { margin-bottom:0px !important; }');
		addGlobalStyle('.main-chat-panel .form-control { width:98%; }');

		//***Main Page Header***
		//Header Color - Background
		if(storage.structure.data.themeData.mainHeaderBg.enabledBool == "True") addGlobalStyle('.navbar.navbar-default.navbar-fixed-top { background-color: '+storage.structure.data.themeData.mainHeaderBg.overrideColor+' !important;}');
		//Header Color - Text
		if(storage.structure.data.themeData.mainHeaderText.enabledBool == "True") addGlobalStyle('.navbar-nav>li>a { color: '+storage.structure.data.themeData.mainHeaderText.overrideColor+' !important;}');
		//Header Color - Border
		if(storage.structure.data.themeData.mainHeaderBorder.enabledBool == "True") addGlobalStyle('.navbar.navbar-default.navbar-fixed-top { border-color: '+storage.structure.data.themeData.mainHeaderBorder.overrideColor+' !important;}');
		
		//***Main Page Footer***
		//Footer Color - Background
		if(storage.structure.data.themeData.mainFooterBg.enabledBool == "True") addGlobalStyle('.navbar.navbar-default.navbar-fixed-bottom { background-color: '+storage.structure.data.themeData.mainFooterBg.overrideColor+' !important;}');
		//Footer Color - Border
		if(storage.structure.data.themeData.mainFooterBorder.enabledBool == "True") addGlobalStyle('.navbar.navbar-default.navbar-fixed-bottom { border-color: '+storage.structure.data.themeData.mainFooterBorder.overrideColor+' !important;}');
		
		
		//**Panels***
		//Default Panels - Background
		if(storage.structure.data.themeData.panelHeaderBg.enabledBool == "True") addGlobalStyle('.panel-heading { background-color: '+storage.structure.data.themeData.panelHeaderBg.overrideColor+' !important;}');
		if(storage.structure.data.themeData.panelBodyBg.enabledBool == "True") addGlobalStyle('.panel-body { background-color: '+storage.structure.data.themeData.panelBodyBg.overrideColor+' !important;}');
		if(storage.structure.data.themeData.panelFooterBg.enabledBool == "True") addGlobalStyle('.panel-footer { background-color: '+storage.structure.data.themeData.panelFooterBg.overrideColor+' !important;}');

		
		//reset body height to 100% of window
		addGlobalStyle('html, body { height: 100%; }');
		if(storage.structure.data.themeData.mainBg.enabledBool == "True") addGlobalStyle('body>.container-fluid:first-of-type { background-image: none !important; background-color: '+storage.structure.data.themeData.mainBg.overrideColor+' !important; min-height:100vh !important; }');
		addGlobalStyle('body>.container-fluid:first-of-type { background-size:cover; }');
		
		//change colors
		addGlobalStyle('body { color:'+storage.structure.data.themeData.normalText.overrideColor+' !important; }');
		//addGlobalStyle('.btn,.required-ingredient { background-color: '+chemo.btnColor+'; border-color: #333; color:#333}');
		//addGlobalStyle('.btn:hover { background-color: '+chemo.btnColor+'; border-color: black; filter: brightness(120%); }');
		//addGlobalStyle('.btn-default { background-color:'+chemo.defaultBtnColor+'; color:#333}');
		//addGlobalStyle('.btn-default:hover { background-color:'+chemo.defaultBtnColor+';}');
		//addGlobalStyle('.required-ingredient { background-color: '+chemo.btnColor+'; border-color: #333; color:#333}');
		//addGlobalStyle('.required-ingredient:hover { background-color: '+chemo.btnColor+'; border-color: black; filter: brightness(120%); }');
		//addGlobalStyle('.btn-info { background-color:'+chemo.infoBtnColor+' !important;  color: white}');
		//addGlobalStyle('.btn-info:hover { background-color:'+chemo.infoBtnColor+' !important;}');
		//addGlobalStyle('.btn-primary { background-color:'+chemo.primaryBtnColor+' !important; color: white}');
		//addGlobalStyle('.btn-primary:hover { background-color:'+chemo.primaryBtnColor+' !important;}');
		//addGlobalStyle('.btn-danger { background-color: '+chemo.badBtnColor+' !important; color: white}');
		//addGlobalStyle('.btn-danger:hover { background-color: '+chemo.badBtnColor+' !important;}');
		//addGlobalStyle('.btn-success { background-color:'+chemo.goodBtnColor+' !important; color: white}');
		//addGlobalStyle('.btn-success:hover { background-color:'+chemo.goodBtnColor+' !important;}');
		//addGlobalStyle('.fa-star,.fa-star-half-o { color:'+chemo.starColor+';}');
		//addGlobalStyle('.bg-success { background-color: '+chemo.highlightColor+';}');
		addGlobalStyle('.tier-3 { color: #48f; }');
		addGlobalStyle('.tier-4 { color: #b4e; }');
		addGlobalStyle('.tier-6 { color: #f24; }');
		
		//set static height
		addGlobalStyle('.navbar-fixed-top { height:50px !important }');
		addGlobalStyle('.navbar-fixed-bottom { height:65px !important }');
		addGlobalStyle('.panel-footer { height:143px }');
		addGlobalStyle('.row:first-of-type { margin-left:0 !important; margin-right:0 !important; }');
		addGlobalStyle('body>.container-fluid:first-of-type { margin-top:0 !important; padding-top:50px !important; padding-left:0 !important; padding-bottom:0 !important; background-size:cover; background-position: center center; background-attachment: fixed; }');

		//inventory styles
		addGlobalStyle('.inventory-panel { max-height:none !important; overflow-y:visible !important; }'); //margin-bottom: 75px;
		addGlobalStyle('.inventory-panel>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
		addGlobalStyle('.components-panel>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
		addGlobalStyle('.components-panel>.panel-heading>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
		addGlobalStyle('.components-panel>.panel-heading:first-of-type { margin-bottom: 40px;  }');
		
		//leaderboard styles
		addGlobalStyle('.leaderboard-panel {overflow-y:visible !important; max-height:none !important; margin-bottom: 75px; }');
		
		//market styles
		//addGlobalStyle('div.col-xs-9.main-page .panel-success {margin-bottom: 75px;}');
		addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row {display:flex;flex-flow: row wrap;}');
		addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row>div.col-md-2 {height:auto !important;}');

		//crafting styles
		addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row>div.col-md-3 {height:auto !important;overflow-y:visible !important;}');
		addGlobalStyle('.recipe-requirements {height:auto !important;overflow-y:visible !important;}');
		addGlobalStyle('.ingredients>div.panel-success>div.panel-body {max-height:none !important; height:auto !important; overflow-y:visible !important;}');
		
		//login styles
		addGlobalStyle('.jumbotron { background-color:rgba(0,0,0,0.75) !important; }');
		
		 //profile styles
		addGlobalStyle('#profile-main-div .col-xs-8 { max-height:none !important; overflow-y:visible !important; margin-bottom:75px;}');
		//addGlobalStyle('.progress-bar {background-color:'+chemo.highlightColor+'; color: black !important;}');
		//addGlobalStyle('.main-page>.well-transparent {margin-bottom: 75px; !important; max-height:100vh !important;}');
		addGlobalStyle('#profile-main-div .col-xs-8 .panel-success .panel-body { max-height:none !important; overflow-y:visible !important;}');
		
		addGlobalStyle('#skill_details .panel-footer {height: auto !important;}');
		addGlobalStyle('#skill_details .panel-body {text-align:center;}');
		addGlobalStyle('#skill_details {width: 450px; max-height:none !important; overflow-y:visible !important;}');
		addGlobalStyle('#dice-roll {width: 300px; margin-left:auto !important; margin-right:auto !important;display:flex;flex-flow:row;justify-content:space-evenly;}');
		//addGlobalStyle('#dice-roll div {width: 37px;padding:0 !important;margin:0 !important;color:'+chemo.diceColor+';}');
		addGlobalStyle('#active_skill_dropdown { color: #ccc !important; }');
		//addGlobalStyle('.dropdown.active-skill-dropdown.bg-success { background-color: black !important; border-left:1px solid '+chemo.highlightColor+';border-right:1px solid '+chemo.highlightColor+'; }');
		
		
		//EXTRA CHEMO CHANGES - REVIEW LATER AND ADD OPTIONS - MOST SEEM BENEFICIAL
		//Hide Chat Heading
		//addGlobalStyle('.main-chat-panel>.panel-heading {display:none}');
		//addGlobalStyle('#messages { height:calc(100vh - 191px) !important; overflow-y:default !important; }');
		//Colorize Stars
		//addGlobalStyle('.quality .fa:nth-of-type(1),.quality .fa:nth-of-type(2),.quality .fa:nth-of-type(3) { color:#0CCC42 !important;}');
		//addGlobalStyle('.quality .fa:nth-of-type(4),.quality .fa:nth-of-type(5),.quality .fa:nth-of-type(6) { color:#48f !important;}');
		//addGlobalStyle('.quality .fa:nth-of-type(7),.quality .fa:nth-of-type(8),.quality .fa:nth-of-type(9) { color:#b4e !important;}');
		//addGlobalStyle('.quality .fa:nth-of-type(10) { color:#D96801 !important;}');
		//addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(1),.dropdown-menu li a .fa:nth-of-type(2),.dropdown-menu li a .fa:nth-of-type(3) { color:#0CCC42 !important;}');
		//addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(4),.dropdown-menu li a .fa:nth-of-type(5),.dropdown-menu li a .fa:nth-of-type(6) { color:#48f !important;}');
		//addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(7),.dropdown-menu li a .fa:nth-of-type(8),.dropdown-menu li a .fa:nth-of-type(9) { color:#b4e !important;}');
		//addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(10) { color:#D96801 !important;}');
		//addGlobalStyle('.popover .fa:nth-of-type(1),.popover .fa:nth-of-type(2),.popover .fa:nth-of-type(3) { color:#0CCC42 !important;}');
		//addGlobalStyle('.popover .fa:nth-of-type(4),.popover .fa:nth-of-type(5),.popover .fa:nth-of-type(6) { color:#48f !important;}');
		//addGlobalStyle('.popover .fa:nth-of-type(7),.popover .fa:nth-of-type(8),.popover .fa:nth-of-type(9) { color:#b4e !important;}');
		//addGlobalStyle('.popover .fa:nth-of-type(10) { color:#D96801 !important;}');
		
		
		
	},
	
	htmlText: function(elementName) {
		var setColor;
		var setDesc;
		var setDefault;
		var setName;
		if(storage.structure.data.themeData[elementName] === undefined) {
			setColor = "undefined";
			setDesc = "undefined";
			setDefault = "undefined";
			setName = "undefined";
		}
		else if(storage.structure.data.themeData[elementName].defaultColor == storage.structure.data.themeData[elementName].overrideColor)
		{
			setColor = storage.structure.data.themeData[elementName].defaultColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		else {
			setColor = storage.structure.data.themeData[elementName].overrideColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		var builder = "";
			builder += '<div id="wrapper">';
			builder += '<tr>';
			builder += '<td class="middle aligned left aligned"><p>'+setDesc+'</p></td>';
			builder += '<td class="middle aligned left aligned"><div id="checkbox'+elementName+'" class="ui fitted toggle checkbox"><input type="checkbox" name="checkbox"><label></label></div></td>';
			builder += '<td class="middle aligned left aligned"><input type=\'text\' class='+setName+' value="'+setColor+'"></td>';
			builder += '<td class="middle aligned left aligned"><p><input id="value'+elementName+'" value="'+setColor+'" style="background-color: '+setColor+'; color: white; text-shadow:-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000;  " ></p></td>';
			builder += '<td class="middle aligned left aligned"><p id="example'+elementName+'" style="color: '+setColor+'">Example Text</p></td>';
			builder += '<td class="middle aligned left aligned"><p>'+setDefault+'</p></td>';
			builder += '</tr>';
			builder += '</div>';
			
		pickersText.push(setName);
		return builder;
	},	
	
	htmlBackground: function(elementName) {
		var setColor;
		var setDesc;
		var setDefault;
		var setName;
		if(storage.structure.data.themeData[elementName] === undefined) {
			setColor = "undefined";
			setDesc = "undefined";
			setDefault = "undefined";
			setName = "undefined";
		}
		else if(storage.structure.data.themeData[elementName].defaultColor == storage.structure.data.themeData[elementName].overrideColor)
		{
			setColor = storage.structure.data.themeData[elementName].defaultColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		else {
			setColor = storage.structure.data.themeData[elementName].overrideColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		var builder = "";
			builder += '<div id="wrapper">';
			builder += '<tr>';
			builder += '<td class="middle aligned left aligned"><p>'+setDesc+'</p></td>';
			builder += '<td class="middle aligned left aligned"><div id="checkbox'+elementName+'" class="ui fitted toggle checkbox"><input type="checkbox" name="checkbox"><label></label></div></td>';
			builder += '<td class="middle aligned left aligned"><input type=\'text\' class='+setName+' value="'+setColor+'"></td>';
			builder += '<td class="middle aligned left aligned"><p><input id="value'+elementName+'" value="'+setColor+'" style="background-color: '+setColor+'; color: white; text-shadow:-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000;  " ></p></td>';
			builder += '<td class="middle aligned left aligned"><p id="example'+setName+'" style="text-align: center; color: rgb(255,255,255); background-color: '+setColor+'">Background</p></td>';
			builder += '<td class="middle aligned left aligned"><p>'+setDefault+'</p></td>';
			builder += '</tr>';
			builder += '</div>';
			
		pickersBackground.push(setName);
		return builder;
	},	
	
	htmlBorder: function(elementName) {
		var setColor;
		var setDesc;
		var setDefault;
		var setName;
		if(storage.structure.data.themeData[elementName] === undefined) {
			setColor = "undefined";
			setDesc = "undefined";
			setDefault = "undefined";
			setName = "undefined";
		}
		else if(storage.structure.data.themeData[elementName].defaultColor == storage.structure.data.themeData[elementName].overrideColor)
		{
			setColor = storage.structure.data.themeData[elementName].defaultColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		else {
			setColor = storage.structure.data.themeData[elementName].overrideColor;
			setDesc = storage.structure.data.themeData[elementName].description;
			setDefault = storage.structure.data.themeData[elementName].defaultColor;
			setName = elementName;
		}
		var builder = "";
			builder += '<div id="wrapper">';
			builder += '<tr>';
			builder += '<td class="middle aligned left aligned"><p>'+setDesc+'</p></td>';
			builder += '<td class="middle aligned left aligned"><div id="checkbox'+elementName+'" class="ui fitted toggle checkbox"><input type="checkbox" name="checkbox"><label></label></div></td>';
			builder += '<td class="middle aligned left aligned"><input type=\'text\' class='+setName+' value="'+setColor+'"></td>';
			builder += '<td class="middle aligned left aligned"><p><input id="value'+elementName+'" value="'+setColor+'" style="background-color: '+setColor+'; color: white; text-shadow:-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000;  " ></p></td>';
			builder += '<td class="middle aligned left aligned"><p id="example'+elementName+'" style="text-align: center; border-style: solid; border-width: 2px; color: rgb(255,255,255); border-color: '+setColor+'">Border Color</p></td>';
			builder += '<td class="middle aligned left aligned"><p>'+setDefault+'</p></td>';
			builder += '</tr>';
			builder += '</div>';
			
		pickersBorder.push(setName);
		return builder;
	},	
};

function addGlobalStyle(css) {
	var head, style;
	head = document.getElementsByTagName('head')[0];
	if (!head) { return; }
	style = document.createElement('style');
	style.type = 'text/css';
	style.innerHTML = css;
	head.appendChild(style);
}

setTimeout(function() {
	start.initialize();
}, 2500);